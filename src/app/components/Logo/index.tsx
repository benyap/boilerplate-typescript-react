import * as React from 'react';

import '../../../assets/images/react.svg';
import './style.scss';

export default () => (
  <img className='logo' src='images/react.svg'/>
);
