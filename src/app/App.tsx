import * as React from 'react';
import Hello from './components/Hello';
import Logo from './components/Logo';

import './global-styles.scss';

const Root = (
  <>
    <Hello/>
    <Logo/>
  </>
);

export default Root;
