describe('greeting', () => {
  it('says hello', () => {
    const greeting: string = 'hello';
    expect(greeting).toEqual('hello');
  });
  it('says goodbye', () => {
    const greeting: string = 'goodbye';
    expect(greeting).toEqual('goodbye');
  });
});
