import * as ReactDOM from 'react-dom';

import App from './app/App';

ReactDOM.render(App, document.getElementById('root'));

// Allow HMR
if (module.hot) {
  module.hot.accept();
}

// Configure if service worker should be used
const USE_SW = false;

// Install SW
if (USE_SW && 'serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/sw.js');
  });
}
