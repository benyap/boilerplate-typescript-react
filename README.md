# Boilerplate - Nodejs with Typescript for React

This is a project which contains all the boilerplate code necessary to kick-start a React project with TypeScript. Comes preinstalled with the following: 

- `webpack` and `webpack-dev-server` to transpile and serve JSX code

- `typescript` build and watch scripts

- `tslint` to ensure a consistent style across the code

- `jest` for testing

- `commitlint` to ensure good commit messages

- `.gitlab-ci.yml` sample configuration to help set up CI/CD on Gitlab


## Project

### Setting up your environment

```bash
# Clone the repository to your local machine
git clone git@gitlab.com:bwyap/boilerplate-nodejs-typescript-react.git

# Step into local repo
cd boilerplate-nodejs-typescript-react

# Install dependencies 
yarn
```

### Running the project

This project can be run in four different modes:

* `local`

* `development`

* `qat`

* `production`

Use the following command to start the project in the specified mode:

```bash
# local (HMR enabled)
yarn watch

# dev
yarn watch:dev

# qat
yarn watch:qat

# production
yarn watch:prod
```


### Building the project

This command will build the project in production mode and output it to the `build` directory

```bash
yarn build
```

You can then run the build using the following command

```bash
yarn start
```

The project can also be built in `local`, `dev` and `qat` environments.
See `package.json` for scripts.


## Contributing

There are a number of conventions that should be followed when developing this project. 
Please see the [CONTRIBUTING](CONTRIBUTING.md) guide for more details. 


## License

This project is licensed under the Apache License, Version 2.0.

See the [LICENSE](LICENSE) for more details. 
