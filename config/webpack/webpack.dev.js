const merge = require('webpack-merge');
const common = require('./webpack.web.js');
const getPlugins = require('./plugins');
const config = require('./config');

// This is the configuration used when building for the development environment
module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  output: {
    filename: `${config.output.js}[name].js`,
    chunkFilename: `${config.output.js}[name].js`,
  },
  plugins: getPlugins(config.tags.dev),
});
