const path = require('path');
const merge = require('webpack-merge');
const base = require('./webpack.base.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const config = require('./config');

/**
 * Configuration for emitting a web bundle
 */

module.exports = merge(base, {
  entry: {
    app: path.join(process.cwd(), config.entry.app),
  },

  output: {
    path: path.resolve(process.cwd(), config.output.directory),
    publicPath: '/',
  },

  plugins: [
    new HtmlWebpackPlugin({
      filename: config.output.html,
      template: config.entry.html
    }),
  ],
});
