const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const RobotsPlugin = require('robotstxt-webpack-plugin').default;

const config = require('./config');

module.exports = (env) => {
  const plugins = [
    new CleanWebpackPlugin([
      config.output.directory
    ]),
    new webpack.DefinePlugin({
      'process.env':{
        'BUILD_DATE': JSON.stringify(new Date()),
        'NODE_ENV': JSON.stringify(env),
      }
    }),
  ];

  // Only allow HMR in dev and local modes
  if (env !== config.tags.prod && env != config.tags.qat) {
    plugins.push(new webpack.HotModuleReplacementPlugin());
  }

  // Stop robots from crawling using robots.txt if not production
  if (env !== config.tags.prod) {
    plugins.push(new RobotsPlugin({
      policy: [{
        userAgent: '*',
        disallow: '/',
      }]
    }));
  }

  return plugins;
}
