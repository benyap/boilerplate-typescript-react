module.exports = {
  entry: {
    // Webpack build entry point
    app: 'src/index.tsx',
    // HtmlWebpackPlugin template file
    html: './src/assets/index.html',
  },
  output: {
    // Output directory
    directory: 'build',
    // HTMLWebpackPlugin output file
    html: 'index.html',
    // WorkboxPlugin service worker file
    sw: 'sw.js',
    // Asset directories
    assets: '',
    fonts: 'fonts/',
    images: 'images/',
    css: 'css/',
    js: 'js/',
  },
  devServer: {
    port: 8080
  },
  tags: {
    local: 'local',
    dev: 'development',
    qat: 'qat',
    prod: 'production',
  }
}
