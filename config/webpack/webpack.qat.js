const merge = require('webpack-merge');
const common = require('./webpack.web.js');
const getPlugins = require('./plugins');
const config = require('./config');

// This is the configuration used when building for the qat environment
module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  output: {
    filename: `${config.output.js}[name].[hash].js`,
    chunkFilename: `${config.output.js}[name].[chunkhash].js`,
  },
  plugins: getPlugins(config.tags.qat),
});
